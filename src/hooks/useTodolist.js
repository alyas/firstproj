import { useContext } from "react";
import TodoContext from "../helper/TodoContext";

export default () => {
    const value = useContext(TodoContext);

    return value;
};