import React from "react";

function Header() {
  return (
    <div className="mt-5 container">
      <h1>Todo List</h1>
    </div>
  );
}

export default Header;
