import React, { useContext } from "react";
import {
  Select,
  MenuItem,
  InputLabel,
  OutlinedInput,
  FormControl,
} from "@mui/material";
import TodoContext from "../helper/TodoContext";

function MoveTodo({
  sublist,
  indexcat,
  indexsublist,
  itemarray,
  setitemarray,
}) {
  const { todoList, setTodoList } = useContext(TodoContext);

  const shiftTodo = (subTodoIndex, categoryIndex) => {
    const getSubTodo = [...sublist];
    const getSubTodoItem = getSubTodo[subTodoIndex];
    const getArraySubtodo = itemarray;
    console.log("getitemsubtodo", getSubTodoItem);
    const getTodoList = [...todoList];
    const filter = getTodoList[indexcat].sublist.filter(
      (value) => !getArraySubtodo.includes(value)
    );

    getTodoList[indexcat].sublist = filter;
    console.log("filter", filter);
    setitemarray([]);
    const newCategory = getTodoList[categoryIndex].sublist;

    getTodoList[categoryIndex].sublist = [...newCategory, ...getTodoList];
    setTodoList(getTodoList);
    console.log("shiftTodo", todoList);
  };

  return (
    <div className="m-0 mx-4">
      <FormControl variant="filled" sx={{ minWidth: 100 }}>
        <InputLabel id="demo-multiple-name-label">Move</InputLabel>
        <Select
          labelId="demo-multiple-name-label"
          id="demo-simple-select-filled"
          input={<OutlinedInput label="Name" />}
        >
          {todoList.map((todoObj, indexDropdown) => (
            <MenuItem
              key={indexDropdown}
              onClick={() => shiftTodo(indexsublist, indexDropdown)}
            >
              {todoObj.todo}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}

export default MoveTodo;
