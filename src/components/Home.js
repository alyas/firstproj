import React, { useContext } from "react";
import "../App.css";
import Form from "./Form";
import TodoList from "./TodoList";
import Header from "./Header";
import TodoContext from "../helper/TodoContext";
import { Button } from "@mui/material";

function Home() {
  const { todoList } = useContext(TodoContext);
  console.log("todoList", todoList);

  const logoutClick = () => {
    localStorage.removeItem("user-data");
    window.location.reload();
  };
  return (
    <div>
      <header className="mt-5">
        <Button
          onClick={logoutClick}
          variant="outlined"
          className="m-3 position-absolute top-0 end-0"
        >
          Logout
        </Button>
        <Header />
        <Form />
        <TodoList />
      </header>
    </div>
  );
}

export default Home;
