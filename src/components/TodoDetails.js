import React, { useContext, useState } from "react";
import "../App.css";
import { useNavigate, useParams } from "react-router-dom";
import Button from "@mui/material/Button";
import TodoContext from "../helper/TodoContext";
import IconButton from "@mui/material/IconButton";
import { Typography, Autocomplete, TextField } from "@mui/material";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import Box from "@mui/material/Box";
import DeleteIcon from "@mui/icons-material/Delete";
import Container from "@mui/material/Container";

function TodoDetails() {
  const navigate = useNavigate();
  const { todoList, setTodoList } = useContext(TodoContext);
  const [sortOrder, setSortOrder] = useState("asc");
  const [subInput, setsubInput] = useState("");
  const [openInput, setopenInput] = useState(false);

  let { todoId } = useParams();
  let list = todoList?.find((list) => list?.id === parseInt(todoId));

  const backbutton = () => {
    navigate("/todolist");
  };

  const [filteredSublist, setFilteredSublist] = useState(list?.sublist || []);

  if (!list || list?.id === undefined) {
    return (
      <div className="d-flex flex-column align-items-center mt-5">
        <Typography variant="h4" align="center" gutterBottom>
          Todo not found.
        </Typography>
        <div className="mt-5">
          <Button variant="outlined" color="secondary" onClick={backbutton}>
            Go Back
          </Button>
        </div>
      </div>
    ); // Render "Todo not found" message
  }

  const handleAutocompleteChange = (event, value) => {
    if (value) {
      const filteredList = list?.sublist?.filter((subtodo) =>
        subtodo.toLowerCase().includes(value.toLowerCase())
      );
      setFilteredSublist(filteredList);
    } else {
      setFilteredSublist(list.sublist);
    }
  };

  const handleSortClick = () => {
    const sortedList = [...filteredSublist];

    if (sortOrder === "asc") {
      sortedList.sort();
      setSortOrder("desc");
    } else {
      sortedList.sort().reverse();
      setSortOrder("asc");
    }

    setFilteredSublist(sortedList);
  };

  const deleteTodo = (sublistIndex) => {
    let getSubtodo = [...list?.sublist];
    const getSubtodoItem = getSubtodo[sublistIndex];
    console.log("getitemsubtodo", getSubtodoItem);
    const getTodoList = [...todoList];

    const filter = getTodoList[todoId].sublist.filter(
      (value) => value !== getSubtodoItem
    );
    getTodoList[todoId].sublist = filter;
    setTodoList(getTodoList);
    setFilteredSublist(getTodoList[todoId].sublist);
  };

  const addTodoClick = (e) => {
    setopenInput(!openInput);

    e.preventDefault();
    if (subInput === "") {
      return alert("Please add something to-do");
    }

    const getTodo = [...todoList];
    getTodo[todoId].sublist = [...getTodo[todoId].sublist, subInput];

    console.log("first", getTodo);
    setTodoList(getTodo);
    setsubInput("");
    setFilteredSublist(getTodo[todoId].sublist);
  };

  return (
    <Container maxWidth="md" className="my-5">
      <Typography variant="h2" align="center" gutterBottom>
        Todo Details
      </Typography>
      <Typography variant="h4" align="center" gutterBottom>
        Category: {list?.todo}
      </Typography>

      <div className="my-5 row">
        <div className="col-1">
          <Typography variant="h5">Todo</Typography>
        </div>
        <div className="col-4">
          {filteredSublist?.length > 0 && (
            <Button variant="outlined" onClick={handleSortClick}>
              Sort {sortOrder === "asc" ? "Ascending" : "Descending"}
            </Button>
          )}
        </div>
        <div className="col-7 d-flex justify-content-end">
          <Autocomplete
            disablePortal
            options={list?.sublist}
            value=""
            size="small"
            sx={{ width: 300 }}
            freeSolo
            onInputChange={handleAutocompleteChange}
            renderInput={(params) => (
              <TextField {...params} label="Search Todo" />
            )}
          />
        </div>
      </div>

      {openInput ? (
        //dislay input to add todo
        <>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 3, width: 0.8 },
            }}
          >
            <TextField
              label="Add todo here"
              variant="outlined"
              value={subInput}
              onChange={(e) => setsubInput(e.target.value)}
              style={{ textAlign: "center" }}
            />
          </Box>
          <Button
            onClick={addTodoClick}
            variant="outlined"
            className="mt-2"
            startIcon={<AddOutlinedIcon />}
          >
            Add Todo
          </Button>
        </>
      ) : (
        //display add todo button (default)
        <div className="d-flex justify-content-end">
          <Button
            className="mt-2"
            onClick={() => setopenInput(!openInput)}
            variant="outlined"
            startIcon={<AddOutlinedIcon />}
          >
            Add Todo
          </Button>
        </div>
      )}

      {filteredSublist?.length > 0 ? (
        <ul className="sublist">
          {filteredSublist.map((subtodo, index) => (
            <li key={subtodo} className="sublist-item">
              {subtodo}
              <IconButton
                aria-label="delete"
                size="large"
                onClick={() => deleteTodo(index)}
                className="mb-1"
              >
                <DeleteIcon />
              </IconButton>
            </li>
          ))}
        </ul>
      ) : (
        <Typography variant="h6" className="my-5">
          None
        </Typography>
      )}

      <Box display="flex" justifyContent="center" mt={5}>
        <Button
          variant="outlined"
          color="secondary"
          onClick={backbutton}
          className="mt-5 justify-content-center"
        >
          Go Back
        </Button>
      </Box>
    </Container>
  );
}

export default TodoDetails;
