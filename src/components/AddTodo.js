import React, { useState, useEffect, Fragment, useContext } from "react";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import Checkbox from "@mui/material/Checkbox";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import MoveTodo from "./MoveTodo";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import TodoContext from "../helper/TodoContext";

function AddTodo({ sublist, indexcat }) {
  const [subInput, setsubInput] = useState("");
  const [openInput, setopenInput] = useState(false);
  const storedArray = localStorage.getItem("itemArray");
  const [itemarray, setitemarray] = useState(storedArray || []);
  const { todoList, setTodoList } = useContext(TodoContext);

  useEffect(() => {
    const storedItemArray = localStorage.getItem("itemArray");
    if (storedItemArray) {
      setitemarray(storedItemArray.toString());
    }
  }, []);

  const addTodoClick = (e) => {
    setopenInput(!openInput);

    e.preventDefault();
    if (subInput === "") {
      return alert("Please add something to-do");
    }

    const gettodo = [...todoList];
    gettodo[indexcat].sublist = [...sublist, subInput];

    console.log("first", gettodo);
    setTodoList(gettodo);
    setsubInput("");
  };

  const displaytodoondefault = () => {
    setopenInput(!openInput);
  };

  const deleteTodo = (sublistindex) => {
    let getlistsubtodo = [...sublist];
    const getitemsubtodo = getlistsubtodo[sublistindex];
    console.log("getitemsubtodo", getitemsubtodo);
    const gettodolist = [...todoList];

    const filter = gettodolist[indexcat].sublist.filter(
      (value) => value !== getitemsubtodo
    );
    gettodolist[indexcat].sublist = filter;

    setitemarray([]);
    setTodoList(gettodolist);
  };

  const movemultipletodo = (e, indexsublist) => {
    if (e.target.checked) {
      console.log("✅ Checkbox is checked");
      const newtodo = [...todoList];
      const itemtodo = newtodo[indexcat].sublist[indexsublist];

      setitemarray((value) => [...value, itemtodo]);
    } else {
      console.log("⛔️ Checkbox is NOT checked");
      const newtodo = [...todoList];
      const itemtodo = newtodo[indexcat].sublist[indexsublist];

      setitemarray([...todoList.filter((value) => value !== itemtodo)]);
    }
  };

  useEffect(() => {
    const json = itemarray.toString();
    const withoutQuotes = json.replace(/\\|"/g, "");
    localStorage.setItem("itemArray", withoutQuotes);
  }, [itemarray]);

  return (
    <>
      {openInput ? (
        //dislay input to add todo
        <Fragment>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 3, width: 0.8 },
            }}
          >
            <TextField
              label="Add todo here"
              variant="outlined"
              value={subInput}
              onChange={(e) => setsubInput(e.target.value)}
              style={{ textAlign: "center" }}
            />
          </Box>
          <Button
            onClick={addTodoClick}
            variant="outlined"
            className="mt-2"
            startIcon={<AddOutlinedIcon />}
          >
            Add Todo
          </Button>
        </Fragment>
      ) : (
        //display add todo button (default)
        <Fragment>
          <Button
            className="mt-2"
            onClick={displaytodoondefault}
            variant="outlined"
            startIcon={<AddOutlinedIcon />}
          >
            Add Todo
          </Button>
        </Fragment>
      )}

      {sublist.map((subtodo, indexsublist) => (
        <Fragment key={indexsublist}>
          <div className="container-2 mt-7">
            <h4>
              <Checkbox
                checked={itemarray.includes(subtodo)}
                onChange={(e) => movemultipletodo(e, indexsublist)}
              />
              Todo: {subtodo}
              <IconButton
                aria-label="delete"
                size="large"
                onClick={() => deleteTodo(indexsublist)}
                className="mb-1"
              >
                <DeleteIcon />
              </IconButton>
            </h4>
            <MoveTodo
              sublist={sublist}
              indexcat={indexcat}
              indexsublist={indexsublist}
              itemarray={itemarray}
              setitemarray={setitemarray}
            />
          </div>
        </Fragment>
      ))}
    </>
  );
}

export default AddTodo;
