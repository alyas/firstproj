import React, { useState, useContext } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import TodoContext from "../helper/TodoContext";

function Form() {
  const [input, setInput] = useState("");
  const [idCounter, setIdCounter] = useState(3); // Keep track of the ID counter
  const { todoList, setTodoList } = useContext(TodoContext);

  const addCategoryClick = (e) => {
    e.preventDefault();
    if (input === "") {
      return alert("Please add something to-do");
    }

    // Check if the input value already exists in the todoList
    const isDuplicate = todoList.some(
      (item) => item.todo.toLowerCase() === input.toLowerCase()
    );
    if (isDuplicate) {
      setInput("");
      return alert("This todo already exists.");
    }

    setTodoList((prev) => [
      ...prev,
      {
        id: idCounter,
        todo: input,
        complete: false,
        sublist: [],
      },
    ]);
    setInput("");
    setIdCounter((prevId) => prevId + 1);
    console.log(todoList);
  };

  return (
    <div className="align-items-center">
      <Box
        align="center"
        component="form"
        sx={{ "& .MuiTextField-root": { width: 0.8 } }}
        className="mt-5"
      >
        <TextField
          value={input}
          onChange={(e) => setInput(e.target.value)}
          className="input-test "
          placeholder="Enter category here"
          align="center"
        />
      </Box>
      <Box align="center" sx={{ "& button": { m: 6 } }}>
        <Button
          onClick={addCategoryClick}
          variant="outlined"
          sx={{ width: 0.5 }}
        >
          ADD CATEGORY
        </Button>
      </Box>
    </div>
  );
}

export default Form;
