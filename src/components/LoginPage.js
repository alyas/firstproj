import React, { useContext } from "react";
import "../App.css";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import Container from "@mui/material/Container";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import TodoContext from "../helper/TodoContext";
import { Typography } from "@mui/material";

const schema = yup
  .object({
    email: yup.string().email().required(),
    password: yup.string().required(),
  })
  .required();

function LoginPage() {
  const { setisLoggedIn, setusername } = useContext(TodoContext);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const navigate = useNavigate();

  const onSubmit = (data) => {
    setisLoggedIn(true);
    localStorage.setItem("user-data", JSON.stringify(data));
    setusername(data);
    navigate("/todolist");
    //data = null
  };

  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmit)} className="form mt-5">
        <Typography variant="h3" className="mb-5" align="center">
          Login
        </Typography>
        <Box mb={2}>
          <TextField
            variant="outlined"
            label="email"
            fullWidth
            autoComplete="email"
            autoFocus
            {...register("email", {
              validate: (value) =>
                value === "me@example.com" || <p>error message</p>,
            })}
            error={!!errors?.email}
            helperText={errors?.email ? errors.email.message : null}
          />
        </Box>
        <Box mb={2}>
          <TextField
            variant="outlined"
            label="password"
            type="password"
            fullWidth
            autoComplete="password"
            autoFocus
            {...register("password", {
              validate: (value) => value === "123456" || <p>error message</p>,
            })}
            error={!!errors?.password}
            helperText={errors.password?.message}
          />
        </Box>
        <Button type="submit" variant="contained" color="primary" fullWidth>
          Login In / Sign Up
        </Button>
      </form>
    </Container>
  );
}

export default LoginPage;
