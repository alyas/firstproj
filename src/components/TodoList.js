import React, { useState, useContext } from "react";
import { Button, Autocomplete, TextField } from "@mui/material";
import UpdateIcon from "@mui/icons-material/Update";
import DeleteIcon from "@mui/icons-material/Delete";
import ListAltIcon from "@mui/icons-material/ListAltOutlined";
import { useNavigate } from "react-router-dom";

import AddTodo from "./AddTodo";

import TodoContext from "../helper/TodoContext";
import CategorySort from "./CategorySort";

function TodoList() {
  const navigate = useNavigate();

  const { todoList, setTodoList, setcategoryIndex } = useContext(TodoContext);

  const [displayInput, setdisplayInput] = useState(false);
  const [inputUpdate, setInputUpdate] = useState("");
  const [selectedRow, setSelectedRow] = useState(undefined);
  const [prevInput, setprevInput] = useState("");
  const [displayDetails, setdisplayDetails] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(null);

  const navigateTodoDetails = (index) => {
    setdisplayDetails(true);
    setcategoryIndex(index);
    navigate("/tododetails/" + index);
  };

  const onSelectedRow = (index) => {
    setSelectedRow(index);
  };

  const deleteTodo = (todo) => {
    const newTodoItems = todoList.filter((item) => item.todo !== todo);
    setTodoList(newTodoItems);
    console.log("newTodoItems", newTodoItems);
  };

  const updateTodo = (index) => {
    setdisplayInput(!displayInput);
    const newTodoItems = [...todoList];
    if (inputUpdate === "") {
      return alert("Please update to-do");
    }
    let todoObj = {
      id: index,
      todo: inputUpdate,
      complete: false,
      sublist: todoList[index].sublist,
    };
    newTodoItems[index] = todoObj;
    // newTodoItems.splice(index, 1, todoObj);
    setTodoList(newTodoItems);
    setInputUpdate("");
  };

  const displayTodo = (index) => {
    setdisplayInput(!displayInput);
    const newTodoItems = [...todoList];
    let test1 = newTodoItems[index].todo;
    setprevInput(test1);
  };

  return (
    <>
      <div className="container mt-5">
        <div className="row">
          <div className="col-8">
            <Autocomplete
              options={todoList}
              getOptionLabel={(option) => option.todo}
              value={selectedCategory}
              onChange={(event, newValue) => {
                setSelectedCategory(newValue);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Filter Category"
                  variant="outlined"
                />
              )}
            />
          </div>

          <div className="col-4">
            <CategorySort />
          </div>
        </div>
      </div>
      <div className="container mt-5">
        <div className="row">
          {todoList
            .filter((currentObj, index) => {
              if (selectedCategory) {
                return (
                  currentObj.todo.toLowerCase() ===
                  selectedCategory.todo.toLowerCase()
                );
              }
              return true;
            })
            .map((currentObj, index) => (
              <div className="col-lg-4 col-md-6 col-sm-12 mb-3" key={index}>
                <div
                  className="container-1"
                  style={{ minWidth: "300px", minHeight: "300px" }}
                >
                  <div>
                    {displayInput && selectedRow === index ? (
                      // Display input to add todo category here
                      <>
                        <TextField
                          onChange={(e) => {
                            setInputUpdate(e.target.value);
                          }}
                          defaultValue={prevInput}
                        />
                        <Button
                          variant="outlined"
                          startIcon={<UpdateIcon />}
                          onClick={() => {
                            updateTodo(currentObj.id);
                          }}
                        >
                          Update
                        </Button>
                      </>
                    ) : (
                      // Display delete, update buttons
                      <div className="mt-3">
                        <h2>Category : {currentObj.todo}</h2>
                        <div className="buttons">
                          <Button
                            variant="outlined"
                            startIcon={<DeleteIcon />}
                            onClick={() => deleteTodo(currentObj.todo)}
                            className="mt-2"
                          >
                            Delete
                          </Button>
                          <div className="mx-3" />
                          <Button
                            onClick={() => {
                              onSelectedRow(index);
                              displayTodo(index);
                            }}
                            startIcon={<UpdateIcon />}
                            variant="outlined"
                            className="mt-2"
                          >
                            Update
                          </Button>
                          <div className="mx-3" />
                          <Button
                            variant="outlined"
                            className="mt-2"
                            onClick={() => navigateTodoDetails(currentObj.id)}
                            startIcon={<ListAltIcon />}
                          >
                            SHOW DETAILS
                          </Button>
                          <div>
                            <AddTodo
                              sublist={currentObj.sublist}
                              indexcat={index}
                              displayDetails={displayDetails}
                            />
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </>
  );
}

export default TodoList;
