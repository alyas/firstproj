import React, { useContext, useState } from "react";
import Button from "@mui/material/Button";
import TodoContext from "../helper/TodoContext";

const CategorySort = () => {
  const { todoList, setTodoList } = useContext(TodoContext);
  const [sortOrder, setSortOrder] = useState("asc");

  const handleSortClick = () => {
    const sortedList = [...todoList];
    if (sortOrder === "asc") {
      sortedList.sort((a, b) => a.todo.localeCompare(b.todo));
      setSortOrder("desc");
    } else {
      sortedList.sort((a, b) => b.todo.localeCompare(a.todo));
      setSortOrder("asc");
    }
    setTodoList(sortedList);
  };

  return (
    <>
      <Button variant="outlined" onClick={handleSortClick}>
        Sort {sortOrder === "asc" ? "Ascending" : "Descending"}
      </Button>
    </>
  );
};

export default CategorySort;
