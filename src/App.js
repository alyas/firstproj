import React, { useState, useEffect, useMemo } from "react";
import { Home, LoginPage, TodoDetails } from "./components";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import TodoContext from "./helper/TodoContext";

function App() {
  const LOCAL_STORAGE_KEY = "task-list";
  const [isLoggedIn, setisLoggedIn] = useState(false);
  const [todoList, setTodoList] = useState(
    JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || [
      {
        id: 0,
        todo: "Work",
        complete: false,
        sublist: [],
      },
      {
        id: 1,
        todo: "Chores",
        complete: false,
        sublist: [],
      },
      {
        id: 2,
        todo: "Others",
        complete: false,
        sublist: [],
      },
    ]
  );

  const [categoryIndex, setcategoryIndex] = useState(0);
  const [username, setusername] = useState(
    JSON.parse(localStorage.getItem("user-data")) || {}
  );

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todoList));
  }, [todoList]);

  const providervalue = useMemo(
    () => ({
      todoList,
      setTodoList,
      categoryIndex,
      setcategoryIndex,
      isLoggedIn,
      setisLoggedIn,
      username,
      setusername,
    }),
    [
      todoList,
      setTodoList,
      categoryIndex,
      setcategoryIndex,
      isLoggedIn,
      setisLoggedIn,
      username,
      setusername,
    ]
  );

  return (
    <div>
      <TodoContext.Provider value={providervalue}>
        <Router>
          <Routes>
            {username.email === "me@example.com" &&
            username.password === "123456" ? (
              <>
                <Route path="/todolist/" element={<Home />}></Route>
                <Route
                  path="/tododetails/:todoId"
                  element={<TodoDetails />}
                ></Route>
                <Route index element={<Home />} />
                <Route path="/login" element={<LoginPage />} />
              </>
            ) : (
              <>
                <Route path="/login" element={<LoginPage />} />
                <Route path="*" element={<Navigate to="/login" />} />
              </>
            )}
          </Routes>
        </Router>
      </TodoContext.Provider>
    </div>
  );
}

export default App;
